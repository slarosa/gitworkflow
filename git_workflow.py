import sys
import os
sys.path.append('path_to_ext-libs_directory')

from git import *

# edit
username = 'your_github_username'
password = 'your_github_password'
email = 'your_github_email'

repo_dir = '/Users/larosa/dev/QGIS_fork'
branch = 'processing_test'
commit_message = "add polygon to lines test"

# not edit
definition_file = 'python/plugins/processing/tests/testdata/qgis_algorithm_tests.yaml'
repo_qgis = 'https://github.com/qgis/QGIS.git'

# using this only if you already have cloned the project
# repo = Repo.init(repo_dir)

# clone the remote repo
repo = Repo.clone_from("https://" + username + ":" + password + "@github.com/" + username + "/QGIS.git", repo_dir)

# create a new branch
new_branch = repo.create_head(branch) 

# create remote urls
# origin = repo.create_remote('origin', repo.remotes.origin.url)
upstream = repo.create_remote('upstream', repo_qgis)

origin = repo.remotes.origin
upstream = repo.remotes.upstream

# get heads anc checkout new branch
heads = repo.heads
new_branch.checkout()
# pull to branch from qgis
upstream.pull(refspec=heads.master)

# add processing test
test = '''
    - algorithm: qgis:polygonstolines
      name: Test (qgis:polygonstolines)
      params:
        INPUT:
          name: polys.gml
          type: vector
      results:
        OUTPUT:
          name: expected/polys2lines.gml
          type: vector
'''
with open(os.path.join(repo_dir, definition_file), "a") as f:
        f.write('\n\n' + test)

# check status of active branch
repo.is_dirty(untracked_files=True)

# repo.untracked_files()
status = repo.git.status()
untracked_files = []

for l in status.split('\n'):
    if l.strip().startswith('python/plugins'):
        untracked_files.append(l.strip())

untracked_files.append(definition_file)

author = Actor("Author", email)
committer = Actor("Committer", email)

index = repo.index
index.add([f for f in untracked_files])
index.commit('[processing-test] ' + commit_message, author=author, committer=committer)

origin.push(refspec=new_branch)
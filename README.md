### Uno script python per creare una pull request su github al progetto QGIS, per aggiungere degli unit test al plugin processing. All'interno della cartella ext-libs sono incluse tutte le liberie necessarie. Sul sistema deve essere installato Git.

### [Il post dove spiego l'utilizzo dello script](https://slarosagis.wordpress.com/2016/03/29/miglioriamo-qgis-come-testandolo/)